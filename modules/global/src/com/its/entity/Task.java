package com.its.entity;

import com.haulmont.cuba.core.entity.StandardEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Table(name = "ITS_TASK")
@Entity(name = "its_Task")
public class Task extends StandardEntity {
    private static final long serialVersionUID = 8977181367701990399L;

    @NotNull
    @Column(name = "CONDITIONS", nullable = false, length = 4094)
    private String conditions;

    @NotNull
    @Column(name = "TYPE_", nullable = false)
    private Integer type;

    public TaskType getType() {
        return type == null ? null : TaskType.fromId(type);
    }

    public void setType(TaskType type) {
        this.type = type == null ? null : type.getId();
    }

    public String getConditions() {
        return conditions;
    }

    public void setConditions(String conditions) {
        this.conditions = conditions;
    }
}