-- begin ITS_TASK
create table ITS_TASK (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    CONDITIONS varchar(4094) not null,
    TYPE_ integer not null,
    --
    primary key (ID)
)^
-- end ITS_TASK
