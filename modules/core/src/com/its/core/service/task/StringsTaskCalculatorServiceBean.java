package com.its.core.service.task;

import com.its.service.task.StringsTaskCalculatorService;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;

@Service(StringsTaskCalculatorService.NAME)
public class StringsTaskCalculatorServiceBean implements StringsTaskCalculatorService {
    StringsTaskCalculator calc = new StringsTaskCalculator();

    @Override
    public String calc(String data) {
        if (Objects.isNull(data) || data.isEmpty()) {
            throw new IllegalArgumentException("Conditions can't be empty");
        }
        String[] s = data.split(";");
        String[] a1 = extractArray(s[0]);
        String[] a2 = extractArray(s[1]);
        if (a1.length == 0 || a2.length == 0) {
            throw new IllegalArgumentException("Both arrays must be not empty");
        }
        return Arrays.toString(calc.calc(a1, a2));
    }

    private String[] extractArray(String s) {
        s = s.replace("[", "").replace("]", "");
        return Arrays.stream(s.split(","))
                .map(s1 -> s1.trim())
                .filter(s1 -> !s1.isEmpty())
                .collect(Collectors.toList()).toArray(new String[0]);
    }
}
