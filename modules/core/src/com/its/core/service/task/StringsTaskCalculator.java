package com.its.core.service.task;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class StringsTaskCalculator {
    public String[] calc(String[] a1, String[] a2) {
        Set<String> set1 = new TreeSet(Arrays.asList(a1));
        for (Iterator<String> it = set1.iterator(); it.hasNext();) {
            String s1 = it.next();
            boolean contains = false;
            for (String s2 : a2) {
                if (s2.contains(s1)) {
                    contains = true;
                    break;
                }
            }
            if (!contains) {
                it.remove();
            }
        }
        return set1.toArray(new String[0]);
    }
}
