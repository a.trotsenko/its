package com.its.core;

import com.its.core.service.task.StringsTaskCalculator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class StringsTaskCalculatorTest {

    @Test
    public void test() {
        String[] a1 = {"arp", "strong", "live", "777", "arp"};
        String[] a2 = {"lively", "alive", "harp", "sharp", "armstrong"};
        String[] expected = {"arp", "live", "strong"};
        Assertions.assertArrayEquals(expected, new StringsTaskCalculator().calc(a1, a2));
    }
}
