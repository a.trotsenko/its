package com.its.web.screens.task;

import com.haulmont.cuba.gui.components.Component;
import com.haulmont.cuba.gui.components.actions.BaseAction;
import com.its.service.task.TaskCalculatorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Consumer;
import java.util.function.Supplier;

public class CalcButtonAction extends BaseAction {
    private static Logger log = LoggerFactory.getLogger(CalcButtonAction.class);
    Supplier<String> conditionsGetter;
    Consumer<String> resultConsumer;
    TaskCalculatorService calcService;
    Consumer<Throwable> errHandler;
    public CalcButtonAction(Supplier<String> conditionsGetter, Consumer<String> resultConsumer,
                            TaskCalculatorService calcService, Consumer<Throwable> errHandler) {
        super("calcAction");
        this.conditionsGetter = conditionsGetter;
        this.resultConsumer = resultConsumer;
        this.calcService = calcService;
        this.errHandler = errHandler;
    }

    @Override
    public void actionPerform(Component component) {
        try {
            resultConsumer.accept(calcService.calc(conditionsGetter.get()));
        } catch (Throwable e) {
            log.error(e.getMessage(), e);
            errHandler.accept(e);
        }
    }
}
