package com.its.web.screens.task.browse;

import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.components.Component;
import com.haulmont.cuba.gui.components.Table;
import com.haulmont.cuba.gui.components.actions.BaseAction;
import com.haulmont.cuba.gui.screen.*;
import com.its.entity.Task;

import javax.inject.Inject;

@UiController("its_Task.browse")
@UiDescriptor("its_TaskBrowse.xml")
@LookupComponent("tasksTable")
@LoadDataBeforeShow
public class TaskBrowse extends StandardLookup<Task> {
    @Inject Notifications notifications;
    @Inject Table<Task> tasksTable;

    @Subscribe
    protected void onInit(InitEvent event) {
        tasksTable.setItemClickAction(new BaseAction("itemClickAction") {
            @Override
            public void actionPerform(Component component) {
                close();
            }
        });
    }

    public void select() {
        if (getSelectedTask() == null) {
            notifications.create().withCaption("No task selected").show();
            return;
        }
        close();
    }

    private void close() {
        close(new CloseAction() {});
    }

    public Task getSelectedTask() {
        return tasksTable.getSingleSelected();
    }
}