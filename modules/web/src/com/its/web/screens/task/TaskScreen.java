package com.its.web.screens.task;

import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.ScreenBuilders;
import com.haulmont.cuba.gui.components.*;
import com.haulmont.cuba.gui.export.ByteArrayDataProvider;
import com.haulmont.cuba.gui.export.ExportDisplay;
import com.haulmont.cuba.gui.export.ExportFormat;
import com.haulmont.cuba.gui.model.InstanceContainer;
import com.haulmont.cuba.gui.screen.*;
import com.haulmont.cuba.gui.upload.FileUploadingAPI;
import com.its.entity.Task;
import com.its.entity.TaskType;
import com.its.service.task.TaskCalculatorService;
import com.its.web.screens.task.browse.TaskBrowse;

import javax.inject.Inject;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;

@UiController("its_TaskScreen")
@UiDescriptor("TaskScreen.xml")
public class TaskScreen extends Screen {

    @Inject FileUploadingAPI fileUploadingAPI;
    @Inject ExportDisplay exportDisplay;
    @Inject ScreenBuilders screenBuilders;
    @Inject Notifications notifications;
    @Inject DataManager dataManager;

    @Inject TaskCalculatorService calcService;

    @Inject LookupField<TaskType> lf_type;
    @Inject InstanceContainer<Task> taskDc;
    @Inject TextArea<String> ta_conditions;
    @Inject TextArea<String> ta_result;
    @Inject Button btn_calc;
    @Inject FileUploadField uploadField;

    @Subscribe
    protected void onInit(InitEvent event) {
        initTask();
        disableUnsupportedTaskType();
        setDefaultConditions();
        initButtonsActions();
    }

    private void initTask() {
        Task task = dataManager.create(Task.class);
        task.setType(TaskType.STRINGS);
        taskDc.setItem(task);
    }

    private void disableUnsupportedTaskType() {
        lf_type.addValueChangeListener(e -> {
            if (e.getValue().equals(TaskType.NUMBERS)) {
                notifications.create(Notifications.NotificationType.ERROR)
                        .withCaption("Unsupported task type").show();
                lf_type.setValue(e.getPrevValue());
            }
        });
    }

    private void setDefaultConditions() {
        String[] a1 = {"arp", "live", "strong", "jojojo", "arp"};
        String[] a2 = {"lively", "alive", "harp", "sharp", "armstrong"};
        ta_conditions.setValue(Arrays.toString(a1) + ";\r\n" + Arrays.toString(a2));
    }

    private void initButtonsActions() {
        btn_calc.setAction(new CalcButtonAction(ta_conditions::getValue,
                (v) -> ta_result.setValue(v),
                calcService,
                (t) -> notifications.create(Notifications.NotificationType.ERROR)
                        .withCaption(t.getMessage())
                        .show()));
    }

    public void save() {
        Task task = taskDc.getItem();
        task.setConditions(ta_conditions.getValue());
        dataManager.commit(task);
        notifications.create().withCaption("Task saved").show();
    }

    public void load() {
        screenBuilders.screen(this)
                .withScreenClass(TaskBrowse.class)
                .withOpenMode(OpenMode.DIALOG)
                .withAfterCloseListener(e -> {
                    Task task = e.getScreen().getSelectedTask();
                    if (task == null) {
                        return;
                    }
                    ta_conditions.setValue(task.getConditions());
                    lf_type.setValue(task.getType());
                    notifications.create().withCaption("Task loaded").show();
                })
                .show();
    }

    public void exportFile() {
        String v = ta_conditions.getValue();
        if (v == null || v.isEmpty()) {
            notifications.create().withCaption("Conditions are empty").show();
            return;
        }
        byte[] bytes;
        try {
            bytes = v.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
        exportDisplay.show(new ByteArrayDataProvider(bytes), "task.txt", ExportFormat.TEXT);
    }

    @Subscribe("uploadField")
    public void onUploadFieldFileUploadSucceed(FileUploadField.FileUploadSucceedEvent event) {
        ta_conditions.setValue(new String(uploadField.getBytes()));
        notifications.create().withCaption("File imported").show();
    }

    @Subscribe("uploadField")
    public void onUploadFieldFileUploadError(UploadField.FileUploadErrorEvent event) {
        notifications.create()
                .withCaption("File upload error: " + event.getCause().getMessage())
                .show();
    }
}